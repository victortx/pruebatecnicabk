package com.practica.pruebaTecnica.persistence.mapper;

import com.practica.pruebaTecnica.domain.Category;
import com.practica.pruebaTecnica.persistence.entity.Categoria;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

// conversores
@Mapper(componentModel = "spring")
public interface CategoryMapper {
    // se covierte el objeto
    @Mappings({
            @Mapping(source = "idCategoria", target = "categoryId"),
            @Mapping(source = "descripcion", target = "category"),
            @Mapping(source = "estado", target = "active"),
    })
    Category toCategory(Categoria categoria);

    @InheritInverseConfiguration
    @Mapping(target = "productos", ignore = true)
    Categoria toCategoria(Category category);
}
