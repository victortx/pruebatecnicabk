package com.practica.pruebaTecnica.web.security.filter;

import com.practica.pruebaTecnica.domain.service.PruebaUserDetailsService;
import com.practica.pruebaTecnica.web.security.JWTutil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtFilterRequest extends OncePerRequestFilter {

    @Autowired
    private JWTutil jwTutil;

    @Autowired
    private PruebaUserDetailsService pruebaUserDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authorizationHeader = request.getHeader("Authorization");
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer")){
            String Jwt = authorizationHeader.substring(7);
            String userName = jwTutil.extractUserName(Jwt);

            // se verifica que el usuario no existe en el contexto
            if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null){
                UserDetails userDetails = pruebaUserDetailsService.loadUserByUsername(userName);

                // validando token con usuario
                if(jwTutil.validateToken(Jwt, userDetails)){
                    UsernamePasswordAuthenticationToken authToken =
                            new UsernamePasswordAuthenticationToken(
                                    userDetails,
                                    null,
                                    userDetails.getAuthorities()
                            );
                    // informacion de conexion
                    authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                    // se setea en el contexto
                    SecurityContextHolder.getContext().setAuthentication(authToken);
                }
            }
        }

        filterChain.doFilter(request, response);
    }
}
